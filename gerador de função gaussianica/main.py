import matplotlib.pyplot as plt
import numpy as np
import scipy as sp
from scipy import stats
import random
import math

def SPUTINIK(m, s):
    p0, q0 = 0.322232431088, 0.099348462606
    p1, q1 = 1.0, 0.588581570495
    p2, q2 = 0.342242088547, 0.531103462366
    p3, q3 = 0.204231210245e-1, 0.103537752850
    p4, q4 = 0.453642210148e-4, 0.385607006340e-2

    u = random.random()

    if u < 0.5:
        t = math.sqrt(-2.0 * math.log(u))
    else:
        t = math.sqrt(-2.0 * math.log(1.0 - u))

    p = p0 + t*(p1 + t * (p2 + t * (p3 + t * p4)))
    q = q0 +t*(q1 + t * (q2 + t * (q3 + t * q4)))

    if u < 0.5:
        z = (p/q)-t
    else:
        z = t-(p/q)

    return (m+s*z)

def main():

  amostra = [SPUTINIK(0, 1) for _ in range(1000)]

  # cria uma faixa de valores para x
  x = np.linspace(min(amostra), max(amostra), 1000)
  # calcula a função gaussiana para cada valor de x
  gaussiana = 1/(np.sqrt(2*np.pi)) * np.exp(-(x**2)/2)

  plt.hist(amostra, bins=50, density=True, alpha=0.5, label='amostra', color = 'purple')
  plt.plot(x, gaussiana, color='b', label='gaussiana')
  plt.legend()
  plt.show()

if __name__ =="__main__":
  main()
