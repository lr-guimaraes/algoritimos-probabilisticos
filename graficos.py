import matplotlib.pyplot as plt

f = 2
a = 3
b = 5
m = 20
f1 = []
n1 = [] 

for n in range(1,100):
    #f = a*(f[n]+b)/m
    x = f
    f = (a*x+b)%m
    f1.append(f)
    n1.append(n)

plt.plot(n1, f1, color='red', linewidth=1, linestyle='dashed')

# Adicionando título e rótulos nos eixos
plt.title('Leandro Ricardo Guimarães f = (a*f+b) mode m, m =20')
plt.xlabel('n')
plt.ylabel('f')

# Exibindo o gráfico
plt.show()