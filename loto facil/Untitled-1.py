def retogrado(sequencia):
    n = len(sequencia) // 2
    posicoes = []
    i = 0
    estado_atual = []

    if not sequencia:
        return [], []

    estado_anterior, posicoes_anterior = retogrado(dict(list(sequencia.items())[:-2]))

    if list(sequencia.values())[:-2] == ["vazio"]:
        estado_atual = estado_anterior + ["vazio", "cheio"]
    else:
        estado_atual = estado_anterior + ["cheio", "vazio"]

    posicoes.extend((i + 1, n - i))
    posicoes += posicoes_anterior
    i += 1

    return estado_atual, posicoes
sequencia = {0: "cheio", 1: "cheio", 2: "cheio", 3: "cheio", 4: "vazio", 5: "vazio", 6: "vazio", 7: "vazio"}
output, posicoes = retogrado(sequencia)

print(f"Input: {sequencia}")
print(f"Output: {output}")
print(f"Posições iniciais: {list(sequencia.keys())}")
print(f"Posições movidas: {posicoes}")
