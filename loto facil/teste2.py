import numpy as np
import matplotlib.pyplot as plt
import re
import datetime
import calendar
from sklearn.cluster import KMeans
from scipy.stats import poisson


def statistics(sort):
    # Start variables geral
    total_numbers = []
    corref_pd = []
    corvar_pd = []

    # Start variable related to the month
    dates = []
    # read file Txt
    with open('Lotofacil.txt', 'r') as arquivo:
        base = arquivo.readlines()

    # Filling the matrix with the drawn numbers
    for linha in base:
        read = linha.find(')')
        if read >= 0:
            values_str = linha[read+1:].strip()
            values = [int(n) for n in re.findall(r'\d+', values_str)]
            linha = values[:16] 
            total_numbers.append(linha)

    # Creating list of dates
    for linha in base:
        values_str = linha.strip()
        values = re.findall(r'\d{2}/\d{2}/\d{4}', values_str)
        if values:
            dates.append(values[0])

    # 1 - Median and standard deviation of all drawn numbers
    total_median = np.mean(total_numbers)
    total_std = np.std(total_numbers)

    # 3 - Variance of all drawn numbers
    total_var = np.var(total_numbers)
    
    # 4 - Variance of each drawn number
    total_var = np.var(total_numbers, axis=1)

    # 5 - Calculate the correlation between the first drawn number and the others
    for i in range(1, len(total_numbers)):
        corref_pd.append(np.corrcoef(total_numbers[0], total_numbers[i], rowvar=True))

    # 6 - Calculate the covariance between the first drawn number and the others
    for i in range(1, len(total_numbers)):
        corvar_pd.append(np.cov(total_numbers[0], total_numbers[i], rowvar=True))
 
    # 7 - Organize the data by day of the week
    days_week = [[] for _ in range(6)]

    for i, date in enumerate(dates):    
        days_w =  datetime.datetime.strptime(date, '%d/%m/%Y').weekday()       
        if days_w == 0:  # Monday
            days_week[0].append(total_numbers[i])  
        elif days_w == 1:  # Tuesday
            days_week[1].append(total_numbers[i])
        elif days_w == 2:  # Wednesday
            days_week[2].append(total_numbers[i])
        elif days_w == 3:  # Thursday
            days_week[3].append(total_numbers[i]) 
        elif days_w == 4:  # Friday
            days_week[4].append(total_numbers[i])  
        elif days_w == 5:  # Saturday
            days_week[5].append(total_numbers[i])  

    # a - Median and standard deviation per day of the week
    Dmedian_pdW = []
    Dstd_pdW = []
    for i in range(6):
        Dmedian_pdW.append(np.mean(days_week[i], axis=0))
        Dstd_pdW.append(np.std(days_week[i], axis=0))

    # b - Median and standard deviation per drawn number
    Dmedian_pd = []
    Dstd_pd = []
    for i in range(5):
        day_array = np.array(days_week[i])
        day_median = np.mean(day_array, axis=0)
        day_std = np.std(day_array, axis=0)
        Dmedian_pdW.append(day_median)
        Dstd_pdW.append(day_std)

    # c - Variance of the entire data set
    Dvar_e = []
    for i in range(5):
        Dvar_e.append(np.var(days_week[i], axis=1))

    # d - Variance per drawn number
    Dvar_pd = []
    for i in range(5):
        Dvar_pd.append(np.var(days_week[i], axis=0))

    # e - Calculate the correlation between Monday and Wednesday
    Dcorref_MW = []
    for i in range(len(days_week)):
        Dcorref_MW.append(np.corrcoef(days_week[0][i], days_week[2][i], rowvar=True))

    # f - Calculate the covariance between Monday and Wednesday
    Dcovar_MW = []
    for i in range(1, len(days_week)):
        for j in range(len(days_week[i])):
            Dcovar_MW.append(np.cov(days_week[0][i], days_week[2][i], rowvar=True))

    # Organize by month
    month = [[] for _ in range(12)]
    for i, date in enumerate(dates):
        mo = datetime.datetime.strptime(date, '%d/%m/%Y').month
        month[mo-1].append(total_numbers[i])

    # 10 - Check the most drawn numbers
    pd_t = []
    # For the entire dataset
    total_numbers_flattened = np.array(total_numbers).flatten()
    unique_numbers, counts = np.unique(total_numbers_flattened, return_counts=True)
    sorted_counts_indices = np.argsort(-counts)
    sorted_numbers = unique_numbers[sorted_counts_indices]
    sorted_counts = counts[sorted_counts_indices]

    # Frequency ordered by day of the week
    weekday_names = list(calendar.day_name)
    pd_t = []
    for i, day in enumerate(days_week):
        numbers_flattened = np.array(day).flatten()
        unique_numbers, counts = np.unique(numbers_flattened, return_counts=True)
        sorted_counts_indices = np.argsort(-counts)
        sorted_numbers = unique_numbers[sorted_counts_indices]
        sorted_counts = counts[sorted_counts_indices]

    # Frequency ordered by month
    month_names = calendar.month_name[1:]
    pd_m = []
    for i, m in enumerate(month_names):
        numbers_flattened = np.array(month[i]).flatten()
        unique_numbers, counts = np.unique(numbers_flattened, return_counts=True)
        sorted_counts_indices = np.argsort(-counts)
        sorted_numbers = unique_numbers[sorted_counts_indices]
        sorted_counts = counts[sorted_counts_indices]

    num_draws = 3
    numbers_to_draw = 15
    draws = []

    # Calculate the mean value for the Poisson distribution
    # Calculate the mean value for the Poisson distribution
    matriz_transicao = np.zeros((25, 25), dtype=float)
    for numbers in days_week[sort-1]:
        for i in range(len(numbers)):
            for j in range(len(numbers)):
                if i != j:
                    matriz_transicao[numbers[i] - 1][numbers[j] - 1] += 1
    matriz_transicao /= matriz_transicao.sum(axis=1, keepdims=True)  # Normalization
    u = 1 / 25

    # Sorteio usando o processo de Poisson
    shuffled_numbers = np.random.permutation(range(1, 26))[:15]
    prediction_poisson = sorted(shuffled_numbers)

    # print("Sorteio usando o processo de Poisson:")
    # print(prediction_poisson)

    # Sorteio usando a matriz de Markov
    np.random.seed(0)  # Define uma semente fixa para reproduzibilidade

    prediction_poisson = []
    for _ in range(15):
        shuffled_numbers = np.random.permutation(range(1, 26))[:15]
        prediction_poisson.extend(sorted(shuffled_numbers))

    print("Sorteio usando o processo de Poisson:")
    print(sorted(prediction_poisson[:15]))

    # Sorteio usando a matriz de Markov
    np.random.seed(0)  # Define uma semente fixa para reproduzibilidade

    # Encontra o histórico de sorteios do dia da semana selecionado
    history = days_week[sort - 1]
    
    # Cria a matriz de transição a partir do histórico
    matriz_transicao = np.zeros((25, 25), dtype=float)
    for numbers in history:
        for i in range(len(numbers) - 1):
            matriz_transicao[numbers[i] - 1][numbers[i + 1] - 1] += 1

    # Normaliza a matriz de transição
    matriz_transicao /= matriz_transicao.sum(axis=1, keepdims=True)

    # Sorteio usando a matriz de transição
    draw_markov = []
    previous_number = np.random.choice(range(1, 26))
    for _ in range(numbers_to_draw):
        draw_markov.append(previous_number)
        next_probabilities = matriz_transicao[previous_number - 1]
        if np.isnan(next_probabilities).any():
            break
        next_number = np.random.choice(range(1, 26), p=next_probabilities)
        previous_number = next_number

    next_weeks_predictions = []
    for _ in range(3):
        shuffled_numbers = np.random.choice(range(1, 26), size=25, replace=False)
        prediction = shuffled_numbers[:15]
        next_weeks_predictions.append(prediction)

    # Print the predictions for the next 3 weeks

    # print("Sorteio usando a matriz de Markov:")
    # print(sorted(draw_markov[:15]))

def main():
    sort = int(input("Qual dia que deseja ver o sorteio:\n "
                     "1 - Segunda\n"
                     "2 - Terça \n"
                     "3 - Quarta \n"
                     "4 - Quinta \n"
                     "5 - Sexta \n"
                     "6 - Sabado \n"
                      " Opção: "))
    statistics(sort)

if __name__ == '__main__':
    main()
