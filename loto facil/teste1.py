import numpy as np
import matplotlib.pyplot as plt
import re
import datetime
import calendar
from sklearn.cluster import KMeans
from scipy.stats import poisson


def statistics(sort):
    # Start variables geral
    total_numbers = []
    corref_pd = []
    corvar_pd = []

    # Start variable related to the month
    dates = []
    # read file Txt
    with open('Lotofacil.txt', 'r') as arquivo:
        base = arquivo.readlines()

    # Filling the matrix which the dawn number
    for linha in base:
        read = linha.find(')')
        if read >= 0:
            values_str = linha[read+1:].strip()
            values = [int(n) for n in re.findall(r'\d+', values_str)]
            linha = values[:16] 
            total_numbers.append(linha)

        # Creating list of dates
# Creating list of dates
    for linha in base:
        values_str = linha.strip()
        values = re.findall(r'\d{2}/\d{2}/\d{4}', values_str)
        if values:
            dates.append(values[0])

    # 1 - Median and standard deviation of everything prize dawn
    total_median = np.mean(total_numbers)
    total_std = np.std(total_numbers)

    # 3 - Variance of total
    total_var = np.var(total_numbers)
    
    # 4 - Variance of everything prize dawn
    total_var = np.var(total_numbers, axis=1)

    # 5 - calc of correlation between first prize dawn and the second prize down....
    for i in range(1, len(total_numbers)):
        corref_pd.append(np.corrcoef(total_numbers[0], total_numbers[i], rowvar=True))

    # 6 - calc of Covar between first prize dawn and the second prize down....
    for i in range(1, len(total_numbers)):
        corvar_pd.append(np.cov(total_numbers[0], total_numbers[i], rowvar=True))
 
    # 7 - Dressing the base of datas and according to the day of the week of the draw
    days_week = [[] for _ in range(6)]

    for i,date in enumerate(dates):    
        days_w =  datetime.datetime.strptime(date, '%d/%m/%Y').weekday()       
        if days_w == 0: #segunda-feira
            days_week[0].append(total_numbers[i]) #terça-feira
        elif days_w == 1: 
            days_week[1].append(total_numbers[i])
        elif days_w == 2: #quarta-feira
            days_week[2].append(total_numbers[i])
        elif days_w == 3:  # Tuesday
            days_week[3].append(total_numbers[i]) 
        elif days_w == 4:  #Sexta-feira
            days_week[4].append(total_numbers[i])  
        elif days_w == 5:  #Sexta-feira
            days_week[5].append(total_numbers[i])  
  
    # a - Median, standard deviation per prize day of week 
    Dmedian_pdW = []
    Dstd_pdW = []
    for i in range(6):
        Dmedian_pdW.append(np.mean(days_week[i], axis=0))
        Dstd_pdW.append(np.std(days_week[i], axis=0))

    # b - Median, standard deviation per prize down
    Dmedian_pd = []
    Dstd_pd = []
    for i in range(5):
        # Convertendo as sublistas em arrays numpy
        day_array = np.array(days_week[i])
        # Calculando a média e o desvio padrão
        day_median = np.mean(day_array, axis=0)
        day_std = np.std(day_array, axis=0)
        # Armazenando os resultados
        Dmedian_pdW.append(day_median)
        Dstd_pdW.append(day_std)

    # c - Variance of everything data base
    Dvar_e = []
    for i in range(5):
        Dvar_e.append(np.var(days_week[i], axis=1))

    # d - Variance of per prize down
    Dvar_pd = []
    for i in range(5):
        Dvar_pd.append(np.var(days_week[i], axis=0))

    # e - calc of correlation between Monday and Wednesday.
    Dcorref_MW = []
    for i in range(len(days_week)):
        Dcorref_MW.append(np.corrcoef(days_week[0][i], days_week[2][i], rowvar=True))

    # f - calc of Covar between Monday and Wednesday....
    Dcovar_MW = []
    for i in range(1, len(days_week)):
        for j in range(len(days_week[i])):
            Dcovar_MW.append(np.cov(days_week[0][i], days_week[2][i], rowvar=True))

    # d - ordering per month
    month = [[] for _ in range(12)]
    for i, date in enumerate(dates):
        mo = datetime.datetime.strptime(date, '%d/%m/%Y').month
        if mo == 1:  # January
            month[0].append(total_numbers[i])
        elif mo == 2:  # February
            month[1].append(total_numbers[i])
        elif mo == 3:  # March
            month[2].append(total_numbers[i])
        elif mo == 4:  # April
            month[3].append(total_numbers[i])
        elif mo == 5:  # May
            month[4].append(total_numbers[i])
        elif mo == 6:  # June
            month[5].append(total_numbers[i])
        elif mo == 7:  # July
            month[6].append(total_numbers[i])
        elif mo == 8:  # August
            month[7].append(total_numbers[i])
        elif mo == 9:  # September
            month[8].append(total_numbers[i])
        elif mo == 10:  # October
            month[9].append(total_numbers[i])
        elif mo == 11:  # November
            month[10].append(total_numbers[i])
        elif mo == 12:  # December
            month[11].append(total_numbers[i])

         
    # 10 - Verificar quais os números mais sorteados
    #   prints
    pd_t = []
    # em toda a base de dados
    total_numbers_flattened = np.array(total_numbers).flatten()
    unique_numbers, counts = np.unique(total_numbers_flattened, return_counts=True)
    sorted_counts_indices = np.argsort(-counts)  # Índices ordenados de forma decrescente
    sorted_numbers = unique_numbers[sorted_counts_indices]
    sorted_counts = counts[sorted_counts_indices]
  

    # Frequência ordenada por dia da semana
    weekday_names = list(calendar.day_name)
    pd_t = []
    for i, day in enumerate(days_week):
        numbers_flattened = np.array(day).flatten()
        unique_numbers, counts = np.unique(numbers_flattened, return_counts=True)
        sorted_counts_indices = np.argsort(-counts)
        sorted_numbers = unique_numbers[sorted_counts_indices]
        sorted_counts = counts[sorted_counts_indices]


    # Frequência ordenada por mês
    # month_names = calendar.month_name[1:]
    # pd_m = []
    # for i, m in enumerate(month_names):
    #     numbers_flattened = np.array(month[i]).flatten()
    #     unique_numbers, counts = np.unique(numbers_flattened, return_counts=True)
    #     sorted_counts_indices = np.argsort(-counts)
    #     sorted_numbers = unique_numbers[sorted_counts_indices]
    #     sorted_counts = counts[sorted_counts_indices]


    #  Graficos
    # 11 - Processo de Poisson e análise de correlação dos números sorteados nas últimas três semanas
    last_weeks = total_numbers[-3:]  # Obtém os números sorteados nas últimas três semanas
    last_weeks_flattened = np.array(last_weeks).flatten()  # Achatando a matriz de números
    unique_numbers, counts = np.unique(last_weeks_flattened, return_counts=True)
    probabilities = counts / np.sum(counts)  # Probabilidades de cada número ser sorteado

    # Cálculo dos parâmetros do processo de Poisson
    poisson_mean = np.mean(counts)

    # Gerar números sorteados seguindo o processo de Poisson
    poisson_numbers = np.random.choice(unique_numbers, 15, p=probabilities, replace=False)

    # Calcular a matriz de correlação dos números sorteados
    correlation_matrix = np.corrcoef(last_weeks_flattened)

    # Imprimir os resultados
    # print("Probabilidades dos números serem sorteados:")
    # for number, prob in zip(unique_numbers, probabilities):
    #     print(f"Número: {number}, Probabilidade: {prob}")

    print("Números sorteados pelo processo de Poisson:")
    print(sorted(poisson_numbers))

    # print("Matriz de correlação dos números sorteados:")
    # print(correlation_matrix)


def main():
    # sort = int(input("Qual dia que deseja ver o sorteio:\n "
    #                  "1 - Segunda\n"
    #                  "2 - Terça \n"
    #                  "3 - Quarta \n"
    #                  "4 - Quinta \n"
    #                  "5 - Sexta \n"
    #                   " Opção: "))
    statistics(1
               )

if __name__ == '__main__':
    main()
