import numpy as np
import matplotlib.pyplot as plt
import re
import datetime
import calendar
import sympy as sp

def read_data_from_file():
    total_numbers = []
    dates = []

    with open('Lotofacil.txt', 'r') as arquivo:
        base = arquivo.readlines()

    for linha in base:
        read = linha.find(')')
        if read >= 0:
            values_str = linha[read+1:].strip()
            values = [int(n) for n in re.findall(r'\d+', values_str)]
            linha = values[:16] 
            total_numbers.append(linha)

    for linha in base:
        values_str = linha.strip()
        values = re.findall(r'\d{2}/\d{2}/\d{4}', values_str)
        if values:
            dates.append(values[0])

    return total_numbers, dates

def calculate_median_std(total_numbers):
    total_median = np.mean(total_numbers)
    total_std = np.std(total_numbers)
    return total_median, total_std

def calculate_var(total_numbers):
    total_var = np.var(total_numbers)
    return total_var

def calculate_var_per_number(total_numbers):
    total_var = np.var(total_numbers, axis=1)
    return total_var

def calculate_correlation(total_numbers):
    corref_pd = []
    for i in range(1, len(total_numbers)):
        corref_pd.append(np.corrcoef(total_numbers[0], total_numbers[i], rowvar=True))
    return corref_pd

def calculate_covariance(total_numbers):
    corvar_pd = []
    for i in range(1, len(total_numbers)):
        corvar_pd.append(np.cov(total_numbers[0], total_numbers[i], rowvar=True))
    return corvar_pd

def organize_data_by_day(total_numbers, dates):
    days_week = [[] for _ in range(6)]

    for i, date in enumerate(dates):    
        days_w =  datetime.datetime.strptime(date, '%d/%m/%Y').weekday()       
        if days_w == 0:  # Monday
            days_week[0].append(total_numbers[i])  
        elif days_w == 1:  # Tuesday
            days_week[1].append(total_numbers[i])
        elif days_w == 2:  # Wednesday
            days_week[2].append(total_numbers[i])
        elif days_w == 3:  # Thursday
            days_week[3].append(total_numbers[i]) 
        elif days_w == 4:  # Friday
            days_week[4].append(total_numbers[i])  
        elif days_w == 5:  # Saturday
            days_week[5].append(total_numbers[i])  
            
    return days_week

def calculate_median_std_per_day(days_week):
    Dmedian_pdW = []
    Dstd_pdW = []
    for i in range(6):
        Dmedian_pdW.append(np.mean(days_week[i], axis=0))
        Dstd_pdW.append(np.std(days_week[i], axis=0))
    return Dmedian_pdW, Dstd_pdW

def calculate_median_std_per_drawn_number(days_week):
    Dmedian_pd = []
    Dstd_pd = []
    for i in range(5):
        day_array = np.array(days_week[i])
        day_median = np.mean(day_array, axis=0)
        day_std = np.std(day_array, axis=0)
        Dmedian_pd.append(day_median)
        Dstd_pd.append(day_std)
    
    return Dmedian_pd, Dstd_pd


def sorteio():
    total_numbers, dates = read_data_from_file()

    total_median, total_std = calculate_median_std(total_numbers)
    total_var = calculate_var(total_numbers)
    total_var_per_number = calculate_var_per_number(total_numbers)
    corref_pd = calculate_correlation(total_numbers)
    corvar_pd = calculate_covariance(total_numbers)

    days_week = organize_data_by_day(total_numbers, dates)
    Dmedian_pdW, Dstd_pdW = calculate_median_std_per_day(days_week)
    Dmedian_pd, Dstd_pd = calculate_median_std_per_drawn_number(days_week)

    num_draws = 6  # Alterado para 6 sorteios
    numbers_to_draw = 15
    draws = []

    # Calculate the mean value for the Poisson distribution
    matriz_transicao_list = []
    for sort in range(6):
        matriz_transicao = np.zeros((25, 25), dtype=float)
        for numbers in days_week[sort]:
            for i in range(len(numbers)):
                for j in range(len(numbers)):
                    if i != j:
                        matriz_transicao[numbers[i] - 1][numbers[j] - 1] += 1
        matriz_transicao /= matriz_transicao.sum(axis=1, keepdims=True)  # Normalization
        matriz_transicao_list.append(matriz_transicao)

    # Sorteio usando o processo de Poisson
    np.random.seed(0)  # Define uma semente fixa para reproduzibilidade
    last_74_draws = total_numbers[-74:] 
    start_index = 0
    # Definir símbolos
    k = sp.Symbol('k')
    lmbda = sp.Symbol('lambda')
    equation = (lmbda**k * sp.exp(-lmbda)) / sp.factorial(k)
    print("=============================================")
    print(f"Equação: {equation}")
    print("=============================================")
    for sort in range(1): # previsao dos 3 proximos sorteios
        #  achatando os ultimos 74 sorteios
        flattened_draws = np.array(last_74_draws).flatten()

        # Frequencia de cada numero sorteado nos ultimos 74 sorteios
        unique_numbers, counts = np.unique(flattened_draws, return_counts=True)

        # Classificando os números por suas frequências em ordem decrescente
        sorted_indices = np.argsort(-counts)
        sorted_numbers = unique_numbers[sorted_indices]

        end_index = start_index + numbers_to_draw

        # Verifica se o índice final ultrapassa o comprimento do array de números
        if end_index > len(sorted_numbers):
            # Selecionando os números restantes do array de números e completa com os primeiros números
            seed_numbers = np.concatenate((sorted_numbers[start_index:], sorted_numbers[:end_index % len(sorted_numbers)]))
        else:
            # Números apropriados do array de números ordenados
            seed_numbers = sorted_numbers[start_index:end_index]

        # Atualiza o índice inicial para a próxima iteração
        start_index = end_index % len(sorted_numbers)

        shuffled_numbers = np.random.permutation(seed_numbers)
        prediction_poisson = sorted(shuffled_numbers)
        draws.append(prediction_poisson)

        # Substitues o símbolo lambda pela média dos números sorteados
        lmbda_value = np.mean(prediction_poisson)
        equation_with_values = equation.subs([(lmbda, lmbda_value), (k, numbers_to_draw)])

        print(f"Sorteio {sort+1} usando o processo de Poisson:")
        print(f"Números sorteados: {prediction_poisson}")
        print(f"Valor de k: {numbers_to_draw}")
        print(f"Valor de lambda: {lmbda_value}")
        print(f"Substituindo os valores: {equation_with_values}")
        print()    
    # print("Sorteios")
    # for i, draw in enumerate(draws):
    #     print(f"Sorteio {i+1} usando o processo de Poisson:")
    #     print(draw)

    plot_graphs(draws, last_74_draws)

import scipy.stats as stats

def plot_graphs(draws, last_74_draws):
    numbers_to_draw = len(last_74_draws[0])
    x = np.arange(1, numbers_to_draw + 1)  # Eixo x com números sorteados

    # for i, draw in enumerate(draws):
    #     if len(draw) > 0:
    #         y = np.hstack(draw)  # Números sorteados no sorteio atual
    #         plt.plot(x, y, marker='o', label=f"Sorteio {i+1}")

    # plt.xlabel('Números Sorteados')
    # plt.ylabel('Números Sorteados')
    # plt.title('Sorteios - Linha por Número Sorteado')
    # plt.legend()
    # plt.grid(True)

    # # Define os valores e rótulos dos ticks no eixo x
    # # plt.xticks(np.arange(1, 26, 1), np.arange(1, 26, 1))
    # plt.show()

    
    # Gráfico para mostrar o padrao capturado nos ultimos 74 sorteios
    # plt.plot(x, np.vstack(last_74_draws).T, marker='o')
    # plt.xlabel('Sorteios Anteriores')
    # plt.ylabel('Números Sorteados')
    # plt.title('Últimos 74 Sorteios - Linha por Número Sorteado')

    # plt.grid(True)
    # plt.show()
    # numbers_to_draw = len(last_74_draws[0])
    # x = np.arange(1, numbers_to_draw + 1)  # Eixo x com números sorteados


    # for i, draw in enumerate(draws):
    #     if len(draw) > 0:
    #         y = np.hstack(draw)  # Números sorteados no sorteio atual
    #         plt.plot(x, y, marker='o', label=f"Sorteio {i+1}")

    #         # Ajuste da regressão de Poisson
    #         poisson_mean = np.mean(y)
    #         poisson_regr = stats.poisson.pmf(x, poisson_mean)
    #         plt.plot(x + 0.5, poisson_regr, 'r--', label='Regressão de Poisson')

    # plt.xlabel('Números Sorteados')
    # plt.ylabel('Números Sorteados')
    # plt.title('Sorteios - Linha por Número Sorteado')
    # plt.legend()
    # plt.grid(True)
    # plt.xticks(np.arange(1, numbers_to_draw + 1), np.arange(1, numbers_to_draw + 1))

    # plt.show()
    numbers_to_draw = len(last_74_draws[0])
    x = np.arange(1, numbers_to_draw + 1)  # Eixo x com números sorteados

    for i, draw in enumerate(draws):
        if len(draw) > 0:
            y = np.hstack(draw)  # Números sorteados no sorteio atual
            color = plt.cm.tab10(i)  # Cor da linha correspondente ao sorteio
            plt.plot(x, y, label=f"Sorteio {i+1}", linestyle='-', linewidth=1, color=color)

    for i, real in enumerate(real_data):
        y_real = np.array(real)  # Dados reais
        color = plt.gca().lines[i].get_color()  # Cor da linha correspondente ao sorteio
        plt.scatter(x, y_real, marker='o', label=f"Dados Reais {i+1}", color=color)
        if i + 1 == len(draws):
            break

    # Linha tracejada como dispersão
    mean_values = np.mean(last_74_draws, axis=0)
    plt.plot(x, mean_values, 'k--', label='Média Dados Reais', linewidth=1)

    plt.xlabel('Números Sorteados')
    plt.ylabel('Números Sorteados')
    plt.title('Sorteios - Linha por Número Sorteado')
    plt.legend()
    plt.grid(True)
    plt.xticks(np.arange(1, numbers_to_draw + 1), np.arange(1, numbers_to_draw + 1))



    plt.show()
real_data = [
[1, 2, 5, 7, 11, 13, 14, 16, 17, 19, 20, 22, 23, 24, 25],
[1, 3, 4, 5, 6, 8, 9, 11, 15, 16, 18, 21, 23, 24, 25],
[1, 2, 3, 7, 9, 10, 12, 14, 15, 16, 18, 19, 20, 21, 23],
[2, 3, 4, 6, 7, 9, 11, 12, 16, 17, 19, 20, 21, 23, 24],
[2, 3, 5, 6, 8, 9, 12, 14, 15, 16, 18, 19, 21, 24, 25],
[1, 4, 5, 7, 8, 9, 11, 12, 14, 15, 16, 19, 21, 22, 24]] 
def main():
    sorteio()

if __name__ == '__main__':
    main()
