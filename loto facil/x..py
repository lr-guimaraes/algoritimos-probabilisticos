import numpy as np
import matplotlib.pyplot as plt
import re
import datetime
import calendar
from sklearn.cluster import KMeans
from scipy.stats import poisson
from sklearn.ensemble import RandomForestRegressor
from sklearn.linear_model import LogisticRegression


def statistics():
    # Start variables geral
    total_numbers = []
    corref_pd = []
    corvar_pd = []

    # Start variable related to the month
    dates = []
    # read file Txt
    with open('Lotofacil.txt', 'r') as arquivo:
        base = arquivo.readlines()

    # Filling the matrix with the drawn numbers
    for linha in base:
        read = linha.find(')')
        if read >= 0:
            values_str = linha[read+1:].strip()
            values = [int(n) for n in re.findall(r'\d+', values_str)]
            linha = values[:16] 
            total_numbers.append(linha)

    # Creating list of dates
    for linha in base:
        values_str = linha.strip()
        values = re.findall(r'\d{2}/\d{2}/\d{4}', values_str)
        if values:
            dates.append(values[0])

    # 1 - Median and standard deviation of all drawn numbers
    total_median = np.mean(total_numbers)
    total_std = np.std(total_numbers)

    # 3 - Variance of all drawn numbers
    total_var = np.var(total_numbers)
    
    # 4 - Variance of each drawn number
    total_var = np.var(total_numbers, axis=1)

    # 5 - Calculate the correlation between the first drawn number and the others
    for i in range(1, len(total_numbers)):
        corref_pd.append(np.corrcoef(total_numbers[0], total_numbers[i], rowvar=True))

    # 6 - Calculate the covariance between the first drawn number and the others
    for i in range(1, len(total_numbers)):
        corvar_pd.append(np.cov(total_numbers[0], total_numbers[i], rowvar=True))
 
    # 7 - Organize the data by day of the week
    days_week = [[] for _ in range(6)]

    for i, date in enumerate(dates):    
        days_w =  datetime.datetime.strptime(date, '%d/%m/%Y').weekday()       
        if days_w == 0:  # Monday
            days_week[0].append(total_numbers[i])  
        elif days_w == 1:  # Tuesday
            days_week[1].append(total_numbers[i])
        elif days_w == 2:  # Wednesday
            days_week[2].append(total_numbers[i])
        elif days_w == 3:  # Thursday
            days_week[3].append(total_numbers[i]) 
        elif days_w == 4:  # Friday
            days_week[4].append(total_numbers[i])  
        elif days_w == 5:  # Saturday
            days_week[5].append(total_numbers[i])  

    # a - Median and standard deviation per day of the week
    Dmedian_pdW = []
    Dstd_pdW = []
    for i in range(6):
        Dmedian_pdW.append(np.mean(days_week[i], axis=0))
        Dstd_pdW.append(np.std(days_week[i], axis=0))

    # b - Median and standard deviation per drawn number
    Dmedian_pd = []
    Dstd_pd = []
    for i in range(5):
        day_array = np.array(days_week[i])
        day_median = np.mean(day_array, axis=0)
        day_std = np.std(day_array, axis=0)
        Dmedian_pdW.append(day_median)
        Dstd_pdW.append(day_std)

    # c - Variance of the entire data set
    Dvar_e = []
    for i in range(5):
        Dvar_e.append(np.var(days_week[i], axis=1))

    # d - Variance per drawn number
    Dvar_pd = []
    for i in range(5):
        Dvar_pd.append(np.var(days_week[i], axis=0))

    # e - Calculate the correlation between Monday and Wednesday
    Dcorref_MW = []
    for i in range(len(days_week)):
        Dcorref_MW.append(np.corrcoef(days_week[0][i], days_week[2][i], rowvar=True))

    # f - Calculate the covariance between Monday and Wednesday
    Dcovar_MW = []
    for i in range(1, len(days_week)):
        for j in range(len(days_week[i])):
            Dcovar_MW.append(np.cov(days_week[0][i], days_week[2][i], rowvar=True))

    # Organize by month
    month = [[] for _ in range(12)]
    for i, date in enumerate(dates):
        mo = datetime.datetime.strptime(date, '%d/%m/%Y').month
        month[mo-1].append(total_numbers[i])

    # 10 - Check the most drawn numbers
    pd_t = []
    # For the entire dataset
    total_numbers_flattened = np.array(total_numbers).flatten()
    unique_numbers, counts = np.unique(total_numbers_flattened, return_counts=True)
    sorted_counts_indices = np.argsort(-counts)
    sorted_numbers = unique_numbers[sorted_counts_indices]
    sorted_counts = counts[sorted_counts_indices]

    # Frequency ordered by day of the week
    weekday_names = list(calendar.day_name)
    pd_t = []
    for i, day in enumerate(days_week):
        numbers_flattened = np.array(day).flatten()
        unique_numbers, counts = np.unique(numbers_flattened, return_counts=True)
        sorted_counts_indices = np.argsort(-counts)
        sorted_numbers = unique_numbers[sorted_counts_indices]
        sorted_counts = counts[sorted_counts_indices]

    # Frequency ordered by month
    month_names = calendar.month_name[1:]
    pd_m = []
    for i, m in enumerate(month_names):
        numbers_flattened = np.array(month[i]).flatten()
        unique_numbers, counts = np.unique(numbers_flattened, return_counts=True)
        sorted_counts_indices = np.argsort(-counts)
        sorted_numbers = unique_numbers[sorted_counts_indices]
        sorted_counts = counts[sorted_counts_indices]

    num_draws = 6  # Alterado para 6 sorteios
    numbers_to_draw = 15
    draws = []

    # Calculate the mean value for the Poisson distribution
    matriz_transicao_list = []
    for sort in range(6):
        matriz_transicao = np.zeros((25, 25), dtype=float)
        for numbers in days_week[sort]:
            for i in range(len(numbers)):
                for j in range(len(numbers)):
                    if i != j:
                        matriz_transicao[numbers[i] - 1][numbers[j] - 1] += 1
        matriz_transicao /= matriz_transicao.sum(axis=1, keepdims=True)  # Normalization
        matriz_transicao_list.append(matriz_transicao)

    # Sorteio usando o processo de Poisson
    np.random.seed(0)  # Define uma semente fixa para reproduzibilidade
    last_74_draws = total_numbers[-74:] 
    start_index = 0
    for sort in range(1):
        # Flatten the last 74 draws
        flattened_draws = np.array(last_74_draws).flatten()

        # Calculate the frequencies of numbers in the last 74 draws
        unique_numbers, counts = np.unique(flattened_draws, return_counts=True)

        # Sort the numbers by their frequencies in descending order
        sorted_indices = np.argsort(-counts)
        sorted_numbers = unique_numbers[sorted_indices]

        end_index = start_index + numbers_to_draw

        # Verifica se o índice final ultrapassa o comprimento do array de números
        if end_index > len(sorted_numbers):
            # Seleciona os números restantes do array de números e completa com os primeiros números
            seed_numbers = np.concatenate((sorted_numbers[start_index:], sorted_numbers[:end_index % len(sorted_numbers)]))
        else:
            # Seleciona os números apropriados do array de números ordenados
            seed_numbers = sorted_numbers[start_index:end_index]

        # Atualiza o índice inicial para a próxima iteração
        start_index = end_index % len(sorted_numbers)

        shuffled_numbers = np.random.permutation(seed_numbers)
        prediction_poisson = sorted(shuffled_numbers)
        draws.append(prediction_poisson)

        
    for i, draw in enumerate(draws):
        print(f"Sorteio {i+1} usando o processo de Poisson:")
        print(draw)
       # Convertendo os números sorteados em um conjunto de dados para treinamento
    # Convertendo os números sorteados em um conjunto de dados para treinamento
    X_train = np.array(draws)
    y_train = X_train[:, :15]  # Seleciona os primeiros 15 números como y_train

    # Flattening y_train para um array unidimensional
    y_train = y_train.flatten()
    # Treinando o modelo de regressão logística
    logistic_reg = LogisticRegression()
    logistic_reg.fit(X_train_binary, y_train)

    # Obtendo os coeficientes do modelo treinado
    coeficients = logistic_reg.coef_

    # Equação da regressão logística
    equation_terms = []
    for i in range(25):
        term = f"{coeficients[0][i]} * X_{i+1}"
        equation_terms.append(term)
    equation = " + ".join(equation_terms)

    # Exibindo a equação com a descrição de cada termo
    print("Equação da regressão logística:")
    print(f"P(Y=1|X) = 1 / (1 + exp(-({equation})))")
    print("\nDescrição dos termos:")
    for i in range(25):
        print(f"X_{i+1}: Presença ou ausência do número {i+1} no conjunto de números anteriores")

def main():
    statistics()

if __name__ == '__main__':
    main()