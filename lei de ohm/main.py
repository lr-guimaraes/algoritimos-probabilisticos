#simulação lei de ohm


def ohm(tolerance):
    v = 5 # tensao
    R =  100 #ley
    t = 0.05
    i = []

    i_nomi = v/R
    if tolerance is not None:
        t = tolerance / 100 
        i.append( i_nomi - i_nomi*t)
        i.append(i_nomi + i_nomi*t)

    if tolerance is not None:
        print(f"A corrente irá varia de {i[0]}A a {i[1]}A")

    else: 
        print(f"A corrente irá ser de {i_nomi}A")
    
def main():
    option =  input("Deseja mudar a tolerancia de 5%? {S/N}: ")

    if option.upper() == 'S':
        tolecence = float(input("Digite o valor da tolerancia: "))
                                
        ohm(tolecence)
    else:
        ohm(None)
    

if __name__ == '__main__':
    main()
