import random


def circuit(tolerance):
    pontos_dentro = []
    pontos_fora = []
    v = 12 # tensao
    R1 = 2200 #ley
    R2 = 330 #ley
    R3 = 560 #ley
    t = 0
    i = []
    ns = 1000000 # isteraçoes

    i_nomi = v/((1/R1 + 1/R2 + 1/R3)**(-1))
    if tolerance is not None:
        t = tolerance / 100 
        i.append( i_nomi - i_nomi*t)
        i.append(i_nomi + i_nomi*t)

        for _ in range(ns):
            x = random.uniform(i[0],i[1])
            y = random.uniform(0, max(0,v))
            if y <= i_nomi:
                pontos_dentro.append((x, y))
            else:
                pontos_fora.append((x, y))
    else:
        t = tolerance / 100 
        i.append( i_nomi - i_nomi*t)
        i.append(i_nomi + i_nomi*t)

        for _ in range(ns):
            x = random.uniform(i[0],i[1])
            y = random.uniform(0, max(0,v))
            if y <= v/((1/R1 + 1/R2 + 1/R3)**(-1)) * x:
                pontos_dentro.append((x, y))
            else:
                pontos_fora.append((x, y))
    I = (len(pontos_dentro) / ns) * (i_nomi) * v
    print("Corrent:", I)


def main():
    option =  input("Deseja mudar a tolerancia de 5%? {S/N}: ")

    if option.upper() == 'S':
        tolecence = float(input("Digite o valor da tolerancia: "))
                                
        circuit(tolecence)
    else:
        circuit(None)
    

if __name__ == '__main__':
    main()
