import math
import scipy.stats

# 1- Probabilidade de André não ser ferido em s segundos.
def n_ter_fermentos(s):
    lambda_ = 1/20  # Taxa de chegada de carros por segundo
    l = lambda_ * s  # Taxa de chegada de carros em s segundos
    p = scipy.stats.poisson.pmf(k=0, mu=l)  # Probabilidade de nenhum carro chegar
    return p

# 1 Probabilidade de André não ser ferido se ele encontrar no máximo 1 carro em S segundos.
def n_ter_fermentos_unico_carro(s):

    lambda_ = 1/20  # Taxa de chegada de carros por segundo
    l = lambda_ * s  # Taxa de chegada de carros em s segundos
    p0 = scipy.stats.poisson.pmf(k=0, mu=l)  # Probabilidade de nenhum carro chegar
    p1 = scipy.stats.poisson.pmf(k=1, mu=l)  # Probabilidade de um carro chegar
    return p0 + p1

# 3a - Probabilidade de não ocorrer nenhum evento entre 20:00hs e 21:00hs.
def sem_eventos():

    lambda_ = 2  # Taxa de eventos por hora
    p = scipy.stats.poisson.pmf(k=0, mu=lambda_)  # Probabilidade de nenhum evento ocorrer
    return p

# 3b - Tempo esperado para a ocorrência do 4º evento a partir do meio dia.
def tempo_esperado_p_4evento():

    lambda_ = 2  # Taxa de eventos por hora
    expected_time = 4 / lambda_
    return expected_time

# 3c - Probabilidade de ocorrência de 2 ou mais eventos entre 18:00hs e 20:00hs.
def probabilidade_2ou_mais_eventos():

    lambda_ = 2  # Taxa de eventos por hora
    p = 1 - scipy.stats.poisson.cdf(k=1, mu=lambda_ * 2)  # Probabilidade de 2 ou mais eventos ocorrerem
    return p

# 4 - Probabilidade condicional em um processo de Poisson.
def probiblidade_condicional(s, t, k, n):

    p = (math.comb(n, k) * ((s / t) ** k) * ((1 - s / t) ** (n - k)))
    return p

# 5a - Tempo médio entre duas chegadas consecutivas.
def media_entre_os_temps_chegada():

    lambda_ = 3  # Taxa de chegada de solicitações por segundo
    mean_time = 1 / lambda_
    return mean_time

# 5b - Probabilidade de que a próxima chegada demore no máximo t segundos.
def prob_tempo_chegada_em_t(t):

    lambda_ = 3  # Taxa de chegada de solicitações por segundo
    p = 1 - math.exp(-lambda_ * t)
    return p

# 6 - Probabilidade de ocorrer uma chegada em até 1s e em até 20s.
def tempo_de_chegada_entre_1e20():

    lambda_ = 8  # Taxa de chegada de pacotes por minuto
    p_arrival_within_1s = 1 - math.exp(-lambda_ / 60)
    p_arrival_within_20s = 1 - math.exp(-lambda_ / 60 * 20)
    return p_arrival_within_1s, p_arrival_within_20s

# 7a - Tempo médio entre chegadas no servidor Web.
def media_entre_os_temps_chegada_web():

    lambda_ = 80  # Taxa de solicitações por segundo
    mean_time = 1 / lambda_
    return mean_time

# 7b - Probabilidade de a próxima solicitação chegar em menos de 10 milissegundos no servidor Web.
def prob_tempo_chegada_em_10ms():
    lambda_ = 80  # Taxa de solicitações por segundo
    p = 1 - math.exp(-lambda_ * 10e-3)
    return p

# 7c - Probabilidade de que a próxima solicitação demore pelo menos 50 milissegundos no servidor Web.
def prob_tempo_chegada_em_50ms():

    lambda_ = 80  # Taxa de solicitações por segundo
    p = math.exp(-lambda_ * 50e-3)
    return p

#7d - Probabilidade de que a próxima chegada ocorra entre 50 e 100 milissegundos no servidor Web.
def prob_tempo_chegada_em_50_100ms():

    lambda_ = 80  # Taxa de solicitações por segundo
    p = math.exp(-lambda_ * 50e-3) - math.exp(-lambda_ * 100e-3)
    return p

def main():
    # Questão 1
    s_values = [2, 5, 10, 20]
    print("\nQuestão 1\n")
    for s in s_values:
        p = n_ter_fermentos(s)
        print(f"Probabilidade de não ser ferido em {s} segundos: {p:.3f}")

    # Questão 2
    s_values = [5, 10, 20, 30]
    print("\nQuestão 2\n")
    for s in s_values:
        p = n_ter_fermentos_unico_carro(s)
        print(f"Probabilidade de não ser ferido em {s} segundos: {p:.3f}")

    # Questão 3
    p_no_event = sem_eventos()
    expected_time = tempo_esperado_p_4evento()
    p_two_or_more = probabilidade_2ou_mais_eventos()

    print("\nQuestão 3\n")
    print(f"A: Probabilidade de não ocorrer nenhum evento entre 20:00hs e 21:00hs: {p_no_event:.3f}")
    print(f"B: Tempo esperado para a ocorrência do 4º evento a partir do meio dia: {expected_time:.2f} horas")
    print(f"C: Probabilidade de ocorrência de 2 ou mais eventos entre 18:00hs e 20:00hs: {p_two_or_more:.3f}")

    # Questão 4
    s = 2
    t = 5
    k = 1
    n = 3
    p = probiblidade_condicional(s, t, k, n)
    print("\nQuestão 4\n")
    print(f"Probabilidade P[N({s}) = {k} | N({t}) = {n}]: {p:.3f}")

    # Questão 5
    mean_time = media_entre_os_temps_chegada()
    p_arrival_t = prob_tempo_chegada_em_t(5)
    print("\nQuestão 5\n")
    print(f"A: Tempo médio entre duas chegadas consecutivas: {mean_time:.3f} segundos")
    print(f"B: Probabilidade de que a próxima chegada demore no máximo 5 segundos: {p_arrival_t:.3f}")

    # Questão 6
    p_arrival_1s, p_arrival_20s = tempo_de_chegada_entre_1e20()
    print("\nQuestão 6\n")
    print(f"Probabilidade de ocorrer uma chegada em até 1s: {p_arrival_1s:.3f}")
    print(f"Probabilidade de ocorrer uma chegada em até 20s: {p_arrival_20s:.3f}")

    # Questão 7
    mean_time_web = media_entre_os_temps_chegada_web()
    p_arrival_10ms = prob_tempo_chegada_em_10ms()
    p_arrival_after_50ms = prob_tempo_chegada_em_50ms()
    p_arrival_between_50_100ms = prob_tempo_chegada_em_50_100ms()
    print ("\nQuestão 7 \n")
    print(f"A: Tempo médio entre chegadas no servidor Web: {mean_time_web:.3f} segundos")
    print(f"B: Probabilidade de a próxima solicitação chegar em menos de 10 milissegundos: {p_arrival_10ms:.3f}")
    print(f"C: Probabilidade de que a próxima solicitação demore pelo menos 50 milissegundos: {p_arrival_after_50ms:.3f}")
    print(f"D: Probabilidade de que a próxima chegada ocorra entre 50 e 100 milissegundos: {p_arrival_between_50_100ms:.3f}")

if __name__ == "__main__":
    main()