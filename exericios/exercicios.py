import numpy as np
import matplotlib.pyplot as plt

def pula_linha1():
  print("\n\n\n=============================")

def pula_linha2():
  print("=============================")
def cov(x, y):
    xbar, ybar = x.mean(), y.mean()
    return np.sum((x - xbar) * (y - ybar)) / (len(x) - 1)

def cov_mat(X):
    return np.array([[cov(X[0], X[0]), cov(X[0], X[1])],
                     [cov(X[1], X[0]), cov(X[1], X[1])]])

def corr(x, y):
    return np.cov(x, y)[0, 1] / np.sqrt(np.cov(x) * np.cov(y))

print("=============================")
# Exercício 1
np.random.seed(42)
x = np.random.normal(0, 1, 500)
y = np.random.normal(10, 2, 500)
X = np.vstack((x, y)).T

plt.scatter(X[:, 0], X[:, 1])
plt.title('Dados gerados Exercicio 1')
plt.axis('equal')
plt.show()

pula_linha1()

# Exercício 2
cov_matrix = np.cov(X.T)
print("Exercicio 2 -  Matrix:")
print(cov_matrix)
pula_linha2()
pula_linha1()
# Exercício 3
X = X - np.mean(X, 0)
scaling_factors = [0.7, 3.4]
Scale = np.diag(scaling_factors)
Y = X.dot(Scale)

plt.scatter(Y[:, 0], Y[:, 1])
plt.title('Exercio 3 - Dados transformados - Exercicio 3')
plt.axis('equal')
plt.show()

cov_matrix_transformed = np.cov(Y.T)
print("Matris de Covariancia (Dados transformados):")
print(cov_matrix_transformed)

pula_linha2()
pula_linha1()
# Exercício 4
a = np.array([1, 2, 3])
b = np.array([1, 2, 1])
weights = [np.array([1, 1, 1]), np.array([1, 1, 0]), np.array([100, 100, 1]), np.array([1, 1, 100])]

for w in weights:
    cov_result = np.cov(a, b, fweights=w)
    print(f"Exercio 4 - Covariancia (Pesos = {w}):")
    print(cov_result)
pula_linha2()
pula_linha1()
print("Exercicio 5")
#Exercicio 5
def cov(x, y):
    xbar, ybar = x.mean(), y.mean()
    return np.sum((x - xbar) * (y - ybar)) / (len(x) - 1)

def corr(x, y):
    return cov(x, y) / (np.std(x) * np.std(y))

a = np.arange(5)

# Ruído com desvio padrão de 0.1
ruido = np.random.normal(0, 0.1, len(a))
corr_result = corr(a, a + ruido)
print("Correlation (std=0.1):")
print(corr_result)

# Ruído com desvio padrão de 1
ruido = np.random.normal(0, 1, len(a))
corr_result = corr(a, a + ruido)
print("Correlation (std=1):")
print(corr_result)

# Ruído com desvio padrão de 10
ruido = np.random.normal(0, 10, len(a))
corr_result = corr(a, a + ruido)
print("Correlation (std=10):")
print(corr_result)

pula_linha2()
pula_linha1()
# Exercício 6
a = np.array([1, 2, 3])
comparisons = [(a, a), (a, 2 * a), (a, 3 * a), (a, -a), (a, a ** 2), (a, a ** 3)]

for x, y in comparisons:
    cov_result = cov(x, y)
    print(f"Exercio 6 - Covariancia (x, y) = {(x, y)}: {cov_result}")
pula_linha2()
pula_linha1()

# Exercício 7
def corr(x, y):
    covariance = np.cov(x, y)[0, 1]
    std_x = np.std(x)
    std_y = np.std(y)
    return covariance / (std_x * std_y)

x = np.array([1, 2, 3])
y = np.array([2, 4, 6])
corr_result = corr(x, y)
print("Exercio 7- Correção (corr(x, y)):")
print(corr_result)

corr_result_np = np.corrcoef(x, y)[0, 1]
print("Exercio 7- Correlação (np.corrcoef(x, y)):")
print(corr_result_np)

pula_linha2()
pula_linha1()

# Exercício 8
correlations = []
for x, y in comparisons:
    corr_result = corr(x, y)
    correlations.append(corr_result)

print("Exercicio 8 -Correlação:")
for i, (x, y) in enumerate(comparisons):
    print(f"Correlação (x, y) = {(x, y)}: {correlations[i]}" )
pula_linha2()
pula_linha1()

# Exercício 9
a = np.arange(5)
noises = [np.random.normal(0, 0.1, len(a)),
          np.random.normal(0, 1, len(a)),
          np.random.normal(0, 10, len(a))]

for noise in noises:
    noisy_data = a + noise
    corr_result = corr(a, noisy_data)
    print("Exercicio 9 -Correlação :")
    print(corr_result)
pula_linha2()

