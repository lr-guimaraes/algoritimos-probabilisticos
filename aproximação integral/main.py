import matplotlib.pyplot as plt
import numpy as np
import random

def monte_carlo_integral(f, a, b, ns, option):
    pontos_dentro = []
    pontos_fora = []
    for _ in range(ns):
        x = random.uniform(a, b)
        y = random.uniform(0, max(f(a, option), f(b, option)))
        if y <= f(x, option):
            pontos_dentro.append((x, y))
        else:
            pontos_fora.append((x, y))
    integral = (len(pontos_dentro) / ns) * (b - a) * max(f(a, option), f(b, option))
    return integral, pontos_dentro, pontos_fora

def f(x, option):
    if option == 1:
        return x**2
    elif option == 2:
        return np.sqrt(1 - x**2)
    else:
        raise ValueError("Opção inválida.")

def plot_integral(f, a, b, ns, option):
    integral, pontos_dentro, pontos_fora = monte_carlo_integral(f, a, b, ns, option)

    # função
    x_values = np.linspace(a, b, ns)
    y_values = f(x_values, option)

    # Gráfico
    fig, ax = plt.subplots()
    x_outside, y_outside = zip(*pontos_fora)
    ax.scatter(x_outside, y_outside, color='pink', s=10, label='Pontos Fora')
    x_inside, y_inside = zip(*pontos_dentro)
    ax.scatter(x_inside, y_inside, color='blue', s=10, label='Pontos Dentro')
    ax.fill_between(x_values, y_values, color='c', alpha=0.2, label='Área da Integral')
    if option == 1:
      ax.plot(x_values, y_values, color='black', label='f(x) = integral de x²')
    elif option ==2: 
      ax.plot(x_values, y_values, color='black', label='f(x) = integral de (1-x²)^0,5')
    ax.set_xlim(a, b)
    ax.set_ylim(0, max(f(a, option), f(b, option)))
    ax.legend(loc='upper right')
    if option == 1:
        plt.title(f"Leandro Ricardo Guimarães | f(x) = x² | Integral = {integral}")
    elif option == 2:
        plt.title(f"Leandro Ricardo Guimarães | f(x) = sqrt(1 - x²) | Integral = {integral}")
    plt.show()

def main():
    a = 0
    b = 0
    ns = int(input("Número de pontos: "))
    option = int(input("Escolha a função: \n1 - f(x) = x^2\n2 - f(x) = sqrt(1 - x^2)\nOpção: "))
    if option == 1:
        b = 2
    elif option == 2:
        b = 1
    plot_integral(f, a, b, ns, option)

if __name__ == "__main__":
    main()
