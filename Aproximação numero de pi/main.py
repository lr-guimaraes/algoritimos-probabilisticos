import matplotlib.pyplot as plt
import math
import random

def pi_esimado(interacoes):
    count = 0
    x_dentro = []
    u_dentro = []
    x_fora = []
    y_fora = []
    for i in range(interacoes):
        x = random.uniform(0, 1)
        y = random.uniform(0, 1)
        d = math.sqrt((x-0.5)**2 + (y-0.5)**2)
        if d < 0.5:
            count += 1
            x_dentro.append(x)
            u_dentro.append(y)
        else:
            x_fora.append(x)
            y_fora.append(y)
    pi = 4*(count/interacoes)
    #retorna os valores de pi e os pontos dentro e fora do grafico
    return pi, x_dentro, u_dentro, x_fora, y_fora
#Grafico

def circulo(pi, x_dentro, u_dentro, x_fora, y_fora):
    fig, ax = plt.subplots()
    circ = plt.Circle((0.5, 0.5), radius=0.5, fill=False, color="black")

    ax.add_patch(circ)
    ax.plot(x_dentro, u_dentro, "o", markersize=1, color="lightblue") #pontos dentro
    ax.plot(x_fora, y_fora, "o", markersize=1, color="pink") #pontos fora

    ax.set_aspect("equal", "box")
    plt.title(f"Valor de pi = {pi:.5f}")
    plt.show()

def main():
    interacoes = int(input("Digite o número de iterações: "))
    pi, x_dentro, u_dentro, x_fora, y_fora = pi_esimado(interacoes)
    circulo(pi, x_dentro, u_dentro, x_fora, y_fora)

if __name__ == "__main__":
    main()
