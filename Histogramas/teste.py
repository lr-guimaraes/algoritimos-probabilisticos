import csv
import numpy as np
import re
import matplotlib.pyplot as plt
from scipy.stats import norm

def read_csv():
    sequencia = []
    
    #captura a sequencia de letras
    with open('/content/sample_data/test.csv', mode='r', newline='') as r_arquivo:
        for i, r in enumerate(r_arquivo):
            separador = r.strip().split(';')
            seq_letra = separador[1]
            if i < 40:
                sequencia.append(seq_letra)
    return sequencia

    #escreve a sequencia de letras 
def write_csv(sequencia):
    cont = 0
    with open('/content/sample_data/test.csv', mode='r', newline='') as r_arquivo:
        with open('output.csv', mode='a', newline='') as w_arquivo:
            reader = csv.reader(r_arquivo, delimiter=';')
            writer = csv.writer(w_arquivo, delimiter=';')
            for i, row in enumerate(reader):
                if i < 40:
                    writer.writerow(row)
                else:
                    if cont < 40:
                        row[1] = sequencia[cont]
                        writer.writerow(row)
                        cont += 1
                    else:
                        cont = 0
    #grafico
def hist(letra):
    sequencia = read_csv()
    write_csv(sequencia)
    
    frame = []
    with open('output.csv', mode='r', newline='') as r_arquivo:
        for r in r_arquivo:
            separador = r.strip().split(';')
            seq_letra = separador[1]
            seq_num = separador[0]
          
            if seq_letra == letra:
              seq_num = re.sub('[^0-9]', '', seq_num)    
              frame.append(int(seq_num))
    
    # Normalização dos valores
    media = np.mean(frame)
    desvio_padrao = np.std(frame)
    frame_normalizados = (frame - media) / desvio_padrao
    
    # Plotando o histograma cumulativo com as frequências ordenadas
    plt.hist(frame_normalizados, bins=10, density=True, alpha=0.5, color='blue') #cumulative=True
    plt.xlabel('Valores')
    plt.ylabel('Frequência acumulada')
    plt.title(f'Histograma de "{letra}" | Leandro ricardo guimaraes')
    
    x = np.linspace(frame_normalizados.min(), frame_normalizados.max(), len(frame))
    y = norm.pdf(x, loc=media, scale=desvio_padrao)

    # Plotando a curva normal sobre o histograma
    plt.plot(x, y, 'r', linewidth=2)
    
    plt.show()

def main():
    letra = input('Letra {I,B,P}: ').upper()
    hist(letra)

if __name__ == "__main__":
    main()
im
